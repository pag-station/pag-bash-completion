#!/usr/bin/env bash

# This is a lib for testing bash autocompletion
# It works by mimicking behavior of what complete should do when tab is pressed
# given I could not make compgen work

SUCCESS_COLOR=${SUCCESS_COLOR:-"\e[35m"}
ERROR_COLOR=${ERROR_COLOR:-"\e[31m"}

assert() {
  line=$3

  if test "$1" = "$2"; then
    # shellcheck disable=SC2016
    printf '%b✅ %-30s was autocompleted to `%s`%b\n' "$SUCCESS_COLOR" "\`$line\`" "$1" "\e[0m"
    return 0;
  fi
  
  code=$?
  # shellcheck disable=SC2016
  printf >&2 '%b❌ for test `%s`\n\texpected: `%s`\n\tactual  : `%s`%b\n' "$ERROR_COLOR" "\`$line\`" "$1" "$2" "\e[0m"
  return "$code"
}

parse_string_for_completion() {
  COMP_LINE=$1
  LAST_LETTER=${COMP_LINE: -1};
  read -r -a COMP_WORDS <<<"$COMP_LINE"
  COMP_CWORD="${#COMP_WORDS[@]}"
  if [ "$LAST_LETTER" != ' ' ]; then
    ((COMP_CWORD -= 1))
  fi
}

autocomplete_test() {
  exec=$1
  line=$2
  parse_string_for_completion "$line"
  shift 2
  expected=$*
  local old_state
  old_state=$(set +o | grep nounset | grep -o -- '[-+]o')
  set +o nounset
  $exec
  set "$old_state" nounset
  assert "$expected" "${COMPREPLY[*]}" "$line"
}
