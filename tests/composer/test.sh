#!/usr/bin/env bash

cd "$(dirname "$0")"

set -euo pipefail

source ../bash-completion-unit-framework.sh
source ../../src/composer

export PATH=$PWD:$PATH

err_counter=0

unit() {
  if ! autocomplete_test "_composer" "$@"; then
    ((err_counter += 1))
  fi
}

set +e

unit "composer az" ""
unit "composer ins" "install"
unit "composer update " "symfony/http-kernel titi/popcorn roave/security-advisories titi/do-the-thing titi/yoga"
unit "composer update s" "symfony/http-kernel"
unit "composer update titi" "titi/popcorn titi/do-the-thing titi/yoga"

exit "$err_counter"
