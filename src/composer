#!/bin/bash

_composer() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  local prev="${COMP_WORDS[COMP_CWORD - 1]}"

  if [ "$COMP_CWORD" == 1 ]; then
    base_commands=(
      about
      archive
      browse
      check-platform-reqs
      clear-cache
      clearcache
      config
      create-project
      depends
      diagnose
      dump-autoload
      exec
      fund
      global
      help
      home
      info
      init
      install
      licenses
      list
      outdated
      prohibits
      reinstall
      remove
      require
      run
      run-script
      search
      self-update
      selfupdate
      show
      status
      suggests
      update
      upgrade
      validate
      why
      why-not
    )

    mapfile -t COMPREPLY < <(compgen -W "${base_commands[*]}" -- "${cur}")
    return 0
  fi

  if [ "$COMP_CWORD" == 2 ] && [ "$prev" == "update" ]; then
    mapfile -t packages < <(jq '.require|keys|.[]' "composer.json" 2>/dev/null)
    mapfile -t packagesDev < <(jq '."require-dev"|keys|.[]' "composer.json" 2>/dev/null)
    mapfile -t COMPREPLY < <(compgen -W "${packages[*]} ${packagesDev[*]}" -- "${cur}")
    return 0
  fi

  if [ "$COMP_CWORD" == 2 ] && [ "$prev" == "create-project" ]; then
    available_projects=(
      laravel/laravel
      symfony/skeleton
      symfony/website-skeleton
    )
    mapfile -t COMPREPLY < <(compgen -W "${available_projects[*]}" -- "${cur}")
    return 0
  fi

  COMPREPLY=()
  return 0
}

complete -F _composer composer
