
.PHONY: composer
composer: ~/.local/share/bash-completion/completions/composer	

~/.local/share/bash-completion/completions/%: src/% |~/.local/share/bash-completion/completions
	cp src/"$$(basename $@)" $@

~/.local/share/bash-completion/completions:
	mkdir $@

.git/hooks/pre-commit: tools/pre-commit
	cp $< $@

.git/hooks/commit-msg: tools/commit-msg
	cp $< $@
